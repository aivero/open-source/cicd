open Jobt

type mode = [
  | #docker
  | #command
]

let parseMode = str => {
  switch str {
  | "docker" => #docker
  | _ => #command
  }
}

let hashLength = 4
let hashN = Hash.hashN(_, hashLength)

let handleDuplicates = jobs => {
  jobs
  ->Array.groupBy(((key, _)) => key)
  ->Array.flatMap(((key, group)) => {
    let hashed_jobs = group->Array.map(((key, job)) => (`${key}@${job->hashN}`, job))
    let needs = hashed_jobs->Array.map(((key, _)) => key)->Array.uniq
    switch needs {
    | [_] => group
    | needs => {
        hashed_jobs->Array.concat([
          (
            key,
            {
              ...Jobt.default,
              script: Some(["echo"]),
              tags: Some(["saas-linux-large-amd64"]),
              extends: Some([".git-strat-none"]),
              needs: Some(needs),
            },
          ),
        ])
      }
    }
  })
}

let git_strat_none = (
  ".git-strat-none",
  {
    ...Jobt.default,
    variables: Some(
      [
        ("GIT_STRATEGY", "none"),
      ]->Dict.fromArray,
    ),
  },
)

let load = ints => {
  Async.seq(
    [Command.getJobs, Docker.getJobs]->Array.map(f => ints->f),
  )->Async.map(jobs => jobs->Result.seq->Result.map(
    jobs => [git_strat_none]->Array.concat(jobs->Array.flat->handleDuplicates))
  )
}
