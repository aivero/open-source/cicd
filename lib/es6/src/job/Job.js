// Generated by ReScript, PLEASE EDIT WITH CARE

import * as Jobt from "./Jobt.js";
import * as Curry from "rescript/lib/es6/curry.js";
import * as Docker from "./Docker.js";
import * as Command from "./Command.js";
import * as Dict$Std from "@prefix_re/std/lib/es6/src/Dict.js";
import * as Hash$Std from "@prefix_re/std/lib/es6/src/Deno/Hash.js";
import * as Array$Std from "@prefix_re/std/lib/es6/src/Array.js";
import * as Async$Std from "@prefix_re/std/lib/es6/src/Async.js";
import * as Result$Std from "@prefix_re/std/lib/es6/src/Result.js";
import * as Caml_option from "rescript/lib/es6/caml_option.js";

function parseMode(str) {
  if (str === "docker") {
    return "docker";
  } else {
    return "command";
  }
}

function hashN(__x) {
  return Hash$Std.hashN(__x, 4);
}

function handleDuplicates(jobs) {
  return Array$Std.flatMap(Array$Std.groupBy(jobs, (function (param) {
                    return param[0];
                  })), (function (param) {
                var group = param[1];
                var hashed_jobs = Array$Std.map(group, (function (param) {
                        var job = param[1];
                        return [
                                param[0] + "@" + Hash$Std.hashN(job, 4),
                                job
                              ];
                      }));
                var needs = Array$Std.uniq(Array$Std.map(hashed_jobs, (function (param) {
                            return param[0];
                          })));
                if (needs.length !== 1) {
                  return Array$Std.concat(hashed_jobs, [[
                                param[0],
                                {
                                  extends: [".git-strat-none"],
                                  variables: Jobt.$$default.variables,
                                  image: Jobt.$$default.image,
                                  tags: ["saas-linux-large-amd64"],
                                  before_script: Jobt.$$default.before_script,
                                  script: ["echo"],
                                  after_script: Jobt.$$default.after_script,
                                  needs: needs,
                                  services: Jobt.$$default.services,
                                  cache: Jobt.$$default.cache,
                                  retry: Jobt.$$default.retry,
                                  artifacts: Jobt.$$default.artifacts,
                                  when: Jobt.$$default.when,
                                  allow_failure: Jobt.$$default.allow_failure,
                                  rules: Jobt.$$default.rules,
                                  interruptible: Jobt.$$default.interruptible
                                }
                              ]]);
                } else {
                  return group;
                }
              }));
}

var git_strat_none_1 = {
  extends: Jobt.$$default.extends,
  variables: Caml_option.some(Dict$Std.fromArray([[
              "GIT_STRATEGY",
              "none"
            ]])),
  image: Jobt.$$default.image,
  tags: Jobt.$$default.tags,
  before_script: Jobt.$$default.before_script,
  script: Jobt.$$default.script,
  after_script: Jobt.$$default.after_script,
  needs: Jobt.$$default.needs,
  services: Jobt.$$default.services,
  cache: Jobt.$$default.cache,
  retry: Jobt.$$default.retry,
  artifacts: Jobt.$$default.artifacts,
  when: Jobt.$$default.when,
  allow_failure: Jobt.$$default.allow_failure,
  rules: Jobt.$$default.rules,
  interruptible: Jobt.$$default.interruptible
};

var git_strat_none = [
  ".git-strat-none",
  git_strat_none_1
];

function load(ints) {
  return Async$Std.map(Promise.all(Array$Std.map([
                      Command.getJobs,
                      Docker.getJobs
                    ], (function (f) {
                        return Curry._1(f, ints);
                      }))), (function (jobs) {
                return Result$Std.map(Result$Std.seq(jobs), (function (jobs) {
                              return Array$Std.concat([git_strat_none], handleDuplicates(Array$Std.flat(jobs)));
                            }));
              }));
}

var hashLength = 4;

export {
  parseMode ,
  hashLength ,
  hashN ,
  handleDuplicates ,
  git_strat_none ,
  load ,
  
}
/* git_strat_none Not a pure module */
